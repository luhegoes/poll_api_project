from django.db import connection, models


class OptionManager(models.Manager):
    COUNT_VOTES_SQL = '''
        SELECT COUNT(*)
        FROM polls_vote
        WHERE option_id = %s
    '''

    COUNT_UNIQUE_VOTES_SQL = '''
        SELECT COUNT(*)
        FROM (
            SELECT DISTINCT ip
            FROM polls_vote
            WHERE option_id = %s
        ) AS TMP
    '''

    def count_votes(self, option_pk):
        with connection.cursor() as cursor:
            cursor.execute(
                OptionManager.COUNT_VOTES_SQL, [option_pk]
            )
            row = cursor.fetchone()
        return row[0]

    def count_unique_votes(self, option_pk):
        with connection.cursor() as cursor:
            cursor.execute(
                OptionManager.COUNT_UNIQUE_VOTES_SQL, [option_pk]
            )
            row = cursor.fetchone()
        return row[0]
