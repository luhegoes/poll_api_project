from django.shortcuts import get_object_or_404

from rest_framework import generics

from apps.polls.api.v1.serializers import (PollSerializer, VoteSerializer,
                                           ResultsSerializer)
from apps.polls.models import Poll


class ListCreatePollAPI(generics.ListCreateAPIView):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer


class RetrievePollAPI(generics.RetrieveAPIView):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer


class CreateVoteAPI(generics.CreateAPIView):
    queryset = Poll.objects.all()
    serializer_class = VoteSerializer

    def get_serializer_context(self):
        return {
            'request': self.request,
            'pk': self.kwargs['pk']
        }


class ListResultsAPI(generics.ListAPIView):
    serializer_class = ResultsSerializer

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        poll = get_object_or_404(
            Poll.objects.select_related(),
            pk=pk
        )
        return poll.poll_options.all()
