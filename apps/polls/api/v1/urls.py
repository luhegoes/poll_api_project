from django.conf.urls import url

from apps.polls.api.v1.views import (
    ListCreatePollAPI, RetrievePollAPI, CreateVoteAPI, ListResultsAPI)

urlpatterns = [
    url(
        regex='^polls/$',
        view=ListCreatePollAPI.as_view(),
        name='poll-list-create'
    ),
    url(
        regex='^polls/(?P<pk>[0-9]+)/$',
        view=RetrievePollAPI.as_view()
    ),
    url(
        regex='^polls/(?P<pk>[0-9]+)/vote/$',
        view=CreateVoteAPI.as_view(),
        name='poll-vote-create'
    ),
    url(
        regex='^polls/(?P<pk>[0-9]+)/results/$',
        view=ListResultsAPI.as_view(),
        name='poll-results-list'
    )
]
