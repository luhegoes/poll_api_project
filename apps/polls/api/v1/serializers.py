from django.shortcuts import get_object_or_404

from rest_framework import serializers

from apps.polls.models import Option, Poll, Vote


class PollSerializer(serializers.ModelSerializer):
    options = serializers.CharField()

    def create(self, validated_data):
        name = validated_data.get('name')
        options = validated_data.get('options')
        options_list = options.split(',')

        instance = Poll.objects.create(name=name)

        for option_name in options_list:
            Option.objects.create(poll=instance, name=option_name)

        return instance

    class Meta:
        model = Poll
        fields = ('name', 'options')


class VoteSerializer(serializers.ModelSerializer):
    ip = serializers.IPAddressField()
    option = serializers.IntegerField(write_only=True)

    def validate_option(self, option):
        pk = self.context['pk']
        poll = get_object_or_404(Poll, pk=pk)
        poll_options = poll.poll_options.all()

        self.cached_poll_options = poll_options

        if option >= poll_options.count():
            raise serializers.ValidationError('Option index out of range')

        return option

    def create(self, validated_data):
        option_index = validated_data.get('option')
        ip = validated_data.get('ip')
        poll_options = self.cached_poll_options
        option = poll_options[option_index]

        return Vote.objects.create(option=option, ip=ip)

    class Meta:
        model = Vote
        fields = ('ip', 'option')


class ResultsSerializer(serializers.ModelSerializer):
    votes = serializers.SerializerMethodField()
    unique_votes = serializers.SerializerMethodField()

    def get_votes(self, obj):
        return Option.objects.count_votes(obj.pk)

    def get_unique_votes(self, obj):
        return Option.objects.count_unique_votes(obj.pk)

    class Meta:
        model = Option
        fields = ('name', 'votes', 'unique_votes')
