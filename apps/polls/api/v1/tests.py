from django.core.urlresolvers import reverse
from django.test import TestCase

from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework.test import APIClient

from apps.polls.models import Poll


class CommonSetUp(TestCase):
    def setUp(self):
        self.manga_poll_name = 'What is your favorite manga?'
        self.manga_option_0 = 'One Piece'
        self.manga_option_1 = 'Berserk'
        self.manga_option_2 = 'Naruto'
        self.manga_options = '{},{},{}'.format(
            self.manga_option_0,
            self.manga_option_1,
            self.manga_option_2
        )
        self.client = APIClient()


class PollTestCase(CommonSetUp):
    def test_list_poll(self):
        url = reverse('poll-list-create')
        response = self.client.get(url)
        assert response.status_code == HTTP_200_OK

    def test_create_poll(self):
        url = reverse('poll-list-create')
        response = self.client.post(url, {
            'name': self.manga_poll_name,
            'options': self.manga_options
        })
        assert response.status_code == HTTP_201_CREATED


class VoteTestCase(CommonSetUp):
    def setUp(self):
        super(VoteTestCase, self).setUp()
        self.default_ip = '127.0.0.1'
        self.option_correct_index = 0
        self.option_wrong_index = 3

        url = reverse('poll-list-create')
        self.client.post(url, {
            'name': self.manga_poll_name,
            'options': self.manga_options
        })
        self.poll = Poll.objects.first()

    def test_create_vote(self):
        url = reverse('poll-vote-create', kwargs={'pk': self.poll.pk})
        response = self.client.post(url, {
            'ip': self.default_ip,
            'option': self.option_correct_index
        })
        assert response.status_code == HTTP_201_CREATED

    def test_create_vote_index_out_of_range(self):
        error_msg = 'Option index out of range'
        url = reverse('poll-vote-create', kwargs={'pk': self.poll.pk})
        response = self.client.post(url, {
            'ip': self.default_ip,
            'option': self.option_wrong_index
        })
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert error_msg in response.data['option']


class ResultTestCase(CommonSetUp):
    def setUp(self):
        super(ResultTestCase, self).setUp()

        url = reverse('poll-list-create')
        self.client.post(url, {
            'name': self.manga_poll_name,
            'options': self.manga_options
        })
        self.poll = Poll.objects.first()

    def poll_vote(self, ip, index):
        url = reverse('poll-vote-create', kwargs={'pk': self.poll.pk})
        self.client.post(url, {
            'ip': ip,
            'option': index
        })

    def poll_vote_bulk(self):
        self.poll_vote('127.0.0.1', 0)
        self.poll_vote('127.0.0.1', 0)
        self.poll_vote('127.0.0.2', 0)
        self.poll_vote('127.0.0.2', 0)
        self.poll_vote('127.0.0.3', 0)

        self.poll_vote('127.0.0.1', 1)
        self.poll_vote('127.0.0.2', 1)
        self.poll_vote('127.0.0.3', 1)
        self.poll_vote('127.0.0.3', 1)

        self.poll_vote('127.0.0.1', 2)
        self.poll_vote('127.0.0.2', 2)
        self.poll_vote('127.0.0.3', 2)
        self.poll_vote('127.0.0.4', 2)

    def test_get_results(self):
        self.poll_vote_bulk()
        url = reverse('poll-results-list', kwargs={'pk': self.poll.pk})
        response = self.client.get(url)

        assert response.status_code == HTTP_200_OK
