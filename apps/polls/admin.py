from django.contrib import admin

from apps.polls.models import Poll, Option, Vote


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created', 'modified')


@admin.register(Option)
class OptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'poll', 'created', 'modified')
    list_filter = ('poll',)


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'option', 'ip', 'created', 'modified')
    list_filter = ('option', 'option__poll')
    search_fields = ('ip',)
