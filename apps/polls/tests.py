from django.test import TestCase

from apps.polls.models import Option, Poll, Vote


class OptionTestCase(TestCase):
    def setUp(self):
        self.poll_name = 'PollName'
        self.option_name = 'OptionName'

    def test_unicode(self):
        poll = Poll.objects.create(name=self.poll_name)
        option = Option.objects.create(
            poll=poll,
            name=self.option_name
        )

        assert unicode(option) == self.option_name


class PollTestCase(TestCase):
    def setUp(self):
        self.poll_name = 'PollName'

    def test_unicode(self):
        poll = Poll.objects.create(name=self.poll_name)
        assert unicode(poll) == self.poll_name


class VoteTestCase(TestCase):
    def setUp(self):
        self.poll_name = 'PollName'
        self.option_name = 'OptionName'
        self.default_ip = '127.0.0.1'

    def test_unicode(self):
        poll = Poll.objects.create(name=self.poll_name)
        option = Option.objects.create(poll=poll, name=self.option_name)
        vote = Vote.objects.create(option=option, ip=self.default_ip)

        assert unicode(vote) == 'Vote: {}'.format(vote.id)
