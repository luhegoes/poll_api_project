from django.db import models

from apps.core.models import TimeStampedModel
from apps.polls.managers import OptionManager


class Poll(TimeStampedModel):
    name = models.CharField(max_length=100)

    @property
    def options(self):
        return ','.join(self.poll_options.all().values_list('name', flat=True))

    def __unicode__(self):
        return self.name


class Option(TimeStampedModel):
    poll = models.ForeignKey(Poll, related_name='poll_options')
    name = models.CharField(max_length=100)

    objects = OptionManager()

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('id',)


class Vote(TimeStampedModel):
    option = models.ForeignKey(Option, related_name='option_votes')
    ip = models.GenericIPAddressField()

    def __unicode__(self):
        return 'Vote: {}'.format(self.id)
