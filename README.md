# README #

### What is this repository for? ###

* Simple poll project using django and django-rest-framework.
* List of endpoints:
#### /v1/polls/
#### /v1/polls/<id>/vote
#### /v1/polls/<id>/results
### How do I get set up? ###
* Install requirements inside your virtualenv:
```
pip install -r requirements/local.txt
```

* Set up the postgres db according to config/settings/local.py

* Run tests:

```
python manage.py test
```


### Who do I talk to? ###

* Luis Herrera <luhegoes@gmail.com>